#include <stdio.h>
#include "microshell.h"
#include "msconf.h"
#include "mscmd.h"

typedef struct {
    void (*puts)(char *str);
} USER_OBJECT;

static void uart_putc(char c)
{
   putc(c,stdout);
}

static void uart_puts(char *str)
{
    while (*str) {
        putc(*str++,stdout);
    }
}

static char uart_getc(void)
{
   return getc(stdin);
}

static void utx(char c)
{
    uart_putc(c);
}

static char urx(void)
{
    return uart_getc();
}

static void action_hook(MSCORE_ACTION action)
{
}

static MSCMD_USER_RESULT usrcmd_system(MSOPT *msopt, MSCMD_USER_OBJECT usrobj);
static MSCMD_USER_RESULT usrcmd_config(MSOPT *msopt, MSCMD_USER_OBJECT usrobj);
static MSCMD_USER_RESULT usrcmd_help(MSOPT *msopt, MSCMD_USER_OBJECT usrobj);

static MSCMD_COMMAND_TABLE table[] = {
    {   "system",   usrcmd_system   },
    {   "config",   usrcmd_config   },
    {   "help",     usrcmd_help     },
    {   "?",        usrcmd_help     },
};

int main(void)
{
    char buf[MSCONF_MAX_INPUT_LENGTH];
    MICROSHELL ms;
    MSCMD mscmd;
    USER_OBJECT usrobj = {
        .puts = uart_puts,
    };

    uart_puts(
            "\r\n\r\n"
            "======================================\r\n"
            " MicroShell Simple Example for Native \r\n"
            "======================================\r\n"
            );
    uart_puts(" Type 'help' for a list of commands.\r\n");

    microshell_init(&ms, utx, urx, action_hook);
    mscmd_init(&mscmd, table, sizeof(table) / sizeof(table[0]), &usrobj);

    while (1) {
        MSCMD_USER_RESULT r;
        uart_puts("MicroShell>");
        microshell_getline(&ms, buf, sizeof(buf));
        mscmd_execute(&mscmd, buf, &r);
    }

    return 0;
}

static MSCMD_USER_RESULT usrcmd_system(MSOPT *msopt, MSCMD_USER_OBJECT usrobj)
{
    char buf[MSCONF_MAX_INPUT_LENGTH];
    int argc;
    int i;
    uart_puts("[SYSTEM]\r\n");
    msopt_get_argc(msopt, &argc);
    for (i = 0; i < argc; i++) {
        msopt_get_argv(msopt, i, buf, sizeof(buf));
        uart_puts(" '");
        uart_puts(buf);
        uart_puts("'\r\n");
    }
    return 0;
}

static MSCMD_USER_RESULT usrcmd_config(MSOPT *msopt, MSCMD_USER_OBJECT usrobj)
{
    char buf[MSCONF_MAX_INPUT_LENGTH];
    int argc;
    int i;
    uart_puts("[CONFIG]\r\n");
    msopt_get_argc(msopt, &argc);
    for (i = 0; i < argc; i++) {
        msopt_get_argv(msopt, i, buf, sizeof(buf));
        uart_puts(" '");
        uart_puts(buf);
        uart_puts("'\r\n");
    }
    return 0;
}

static MSCMD_USER_RESULT usrcmd_help(MSOPT *msopt, MSCMD_USER_OBJECT usrobj)
{
    USER_OBJECT *uo = (USER_OBJECT *)usrobj;
    uo->puts(
            "system : system command\r\n"
            "config : config command\r\n"
            "help   : help command\r\n"
            );
    return 0;
}
